import React from 'react';
import {
    Box,
    Card,
    Image,
    Heading,
    Text
} from 'rebass';

const SellerCard = () => {
    return (
        <Box width={256}>
            <Card
                sx={{
                    p: 1,
                    borderRadius: 2,
                    boxShadow: '0 0 16px rgba(0, 0, 0, .25)',
                }}>
                <Image src={''} />
                <Box px={2}>
                    <Heading as='h3'>
                        {'Parchun ki dukan'}
                    </Heading>
                    <Text fontSize={0}>
                        {'Gali me'}
                    </Text>
                </Box>
            </Card>
        </Box>
    )
}

export default SellerCard;
