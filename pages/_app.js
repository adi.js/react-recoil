import React from "react";
import { ThemeProvider } from 'emotion-theming';
import theme from '@rebass/preset';
import { RecoilRoot } from 'recoil';

function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        <RecoilRoot>
          <Component {...pageProps} />
        </RecoilRoot>
      </ThemeProvider>
    </React.Fragment>
  );
}
export default MyApp;
