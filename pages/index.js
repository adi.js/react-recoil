import Head from 'next/head'
import React from 'react';
import { Flex } from 'rebass';
import { useRecoilValue } from 'recoil';
import SellerCard from '../components/seller-card';
import { sellerList } from '../recoil/seller-details';

export default function Home() {
  const sellerlist = useRecoilValue(sellerList);
  return (
    <Flex>
      {sellerlist && sellerlist.length > 0 &&
        sellerlist.map((item) => (
          <SellerCard key={item.id}></SellerCard>
        ))
      }
    </Flex>
  )
}
